var app = require('express')();

var http = require('http').createServer(app);

var io = require('socket.io').listen(http);

var clients = {};

http.listen(3000, function(){

	console.log("Servidor iniciado em localhost: 3000. Aperte Crlt+C");

});

app.get('/', function(req, res){
res.sendFile(__dirname + '/index.html');
});

io.sockets.on('connection', function (socket) {
socket.on('toServer', function (data){
var msg = data.nome+":"+data.msg;
socket.emit('toClient', msg);
socket.broadcast.emit('toClient',msg);
	});
});

io.on("connection", function (client){
	console.log('user connected');
});
