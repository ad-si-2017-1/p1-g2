// Load the TCP Library
net = require('net');
// Keep track of the chat clients
var clients = [];
// Nicknames Map
var nicks = {};

var user = [];

var password = [];

var channels = [];

// Start a TCP Server
net.createServer(function (socket) {
  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort
  // Put this new client in the list
  clients.push(socket);
  // Send a nice welcome message and announce
  socket.write("Welcome " + socket.name + "\n");
  broadcast(socket.name + " joined the chat\n", socket);
  // Handle incoming messages from clients.
  socket.on('data', function (data) {
    // broadcast(socket.name + "> " + data, socket);
    // analisar a mensagem
    analisar(data);
  });
  // Remove the client from the list when it leaves
  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);
    broadcast(socket.name + " left the chat.\n");
  });

  // Send a message to all clients
  function broadcast(message, sender) {
    clients.forEach(function (client) {
      // Don't want to send it to sender
      if (client === sender) return;
      client.write(message);
    });
    // Log it to the server output too
    process.stdout.write(message)
  }
  function analisar(data) {
    var commands = data.toString().split(/\n/).

    for (var i=0; i<commands.length; i++){

      var command = commands[i];
      var args    = command.split(/ :/)[0];
      var message = command.split(args + " :")[1];
      args = args.split(/ :/)[0].split(" ");
    }


    // visualizar os dados como são (sem tratamento)
    // console.log(data);
    // visualizar como string
    // console.log(String(data));
    // visualizar os caracteres especiais enviados
    // console.log(JSON.stringify(String(data)));
    // o método trim remove os caracteres especiais do final da string
//    var mensagem = String(data).trim();
    // o método split quebra a mensagem em partes separadas p/ espaço em branco
//    var args = mensagem.split(" ");
    if ( args[0] == "NICK" ) nick(args); // se o primeiro argumento for NICK
    else if ( args[0] == "USER") user(args);
    else if ( args[0] == "JOIN") join(args);
    else if (args[0] == 'QUIT') quit(args);
    else if (args[0] == 'LIST') list(args));
    else if (args[0] == 'PRIVMSG') privmsg(args);
    else if (args[0] == 'PART') part(args));
    else if (args[0] == 'KICK') part(args));
    else socket.write("ERRO: comando inexistente\n");
  }

  function nick(args) {
      if (!args[1]) {
           socket.write("ERRO: esta faltando nickname\n");
           return;
      }else if ( nicks [args[1]] ){
           socket.write("Erro: nickname já existe\n");
            return;
      }else{
           if (socket.nick){ //entrar na condicao se o nick foi atribuido
             delete nicks[ socket.nick ]; //remove o nick atual do mapa
          }
          //inclui o nick no mapa
          nick [ args[1] ] = socket.name;

          //registra o nick no objeto socket
          socket.nick = args[1];
      }
      socket.write("OK: comando NICK executado com sucesso\n");
  }

  function user(args) {
    for(var i=1; i<user.length; i++){
      if(user[i] == args[1]){
        if (senha[i] == [2]){
          socket.user = args[i];
          socket.write("Login executado!");
          return;
        }else{
          socket.write("Senha incorreta");
          return.
        }
      }else{
        socket.user = args[1];
        senha[user.length] = args[2];
        socket.write("User cadastrado");
      }
    }
    socket.write("OK: comando USER executado com sucesso\n");
  }

  function join(args) {
    socket.channel = args[1];
    socket.write("Canal: " + args[1]);
    broadcast(socket.name + "entrou na sala " + args[1], socket);
    channels = channels + socket.channel;ß
    socket.write("\nOK: comando JOIN executado com sucesso\n");
  }

  function quit(args){

     broadcast("O user " + socket.name + "saiu da sala");
     socket.write("Tchau tchau! Você deixou a sala");
     socket.end();

  }

  function list(args){

    for (var i = 0; i<channels.length; i++){
      socket.write(socket.channel[i] + "\n");
    }
  }

  function privmsg(args){
    clients.forEach(function(client){
      if(client.nick === args[1]){
        client.write("[Mensagem privada para você]");
        client.write(socket.nick + " diz: " + args[2] + "\n");
      }
    });

  }

  function part(args){
    if (!args[1]){
      socket.write("Você esqueceu de informar o nome da Sala");
    }else {
      socket.write("Você saiu da sala " + args[1]);
      socket.channel = '';
    }
  }

  function kick(args){
    clients.forEach(function(client){
      if(args[1] === client.nick){
        if(client.channel === socket.channel){
          socket.write("Você exclui o user: " + client.nick);
          client.write("O user " + socket.nick + " te excluiu");
          broadcast("O user " + socket.nick + "acabou de excluir o user " + client.nick);
          client.nick = '';
        }
      }
    });
  }


}).listen(6667);
// Put a friendly message on the terminal of the server.
console.log("Chat server running at port 6667\n");




